
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innovanglais/fragments/category_pages/list_category.dart';
import 'package:innovanglais/fragments/firm_pages/list_firm.dart';
import 'package:innovanglais/fragments/level_pages/list_level.dart';
import 'package:innovanglais/fragments/payment_pages/payment.dart';
import 'package:innovanglais/fragments/sub_pages/list_sub.dart';
import 'package:innovanglais/fragments/theme_pages/list_theme.dart';
import 'package:innovanglais/fragments/user_pages/list_user.dart';
import 'package:innovanglais/fragments/vocabulary_pages/list_vocabulary.dart';

class AdminListPage extends StatefulWidget {
  @override
  _AdminListPageState createState() => _AdminListPageState();
}

class _AdminListPageState extends State<AdminListPage> {
  @override
  Widget build(BuildContext context) {
    return new Container(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
          Row(
            //ROW 1
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 200, // specific value
                child: RaisedButton(
                  color: Colors.deepPurpleAccent,
                  textColor: Colors.white,
                  onPressed: () {Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CategorieListPage()),
                  );
                  },
                  child:
                      const Text('Catégorie', style: TextStyle(fontSize: 20)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
              SizedBox(
                width: 200, // specific value
                child: RaisedButton(
                  color: Colors.deepPurpleAccent,
                  textColor: Colors.white,
                  onPressed: () {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FirmListPage()),
                  );
                    },
                  child:
                      const Text('Entreprise', style: TextStyle(fontSize: 20)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
          Row(
              //ROW 2
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 200, // specific value

                  child: RaisedButton(
                    color: Colors.deepPurpleAccent,
                    textColor: Colors.white,
                    onPressed: () {Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LevelListPage()),
                    );
                    },
                    child: const Text('Niveau', style: TextStyle(fontSize: 20)),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                ),
                SizedBox(
                  width: 200, // specific value

                  child: RaisedButton(
                    color: Colors.deepPurpleAccent,
                    textColor: Colors.white,
                    onPressed: () {Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SubscriptionListPage()),
                    );
                    },
                    child: const Text('Abonnement',
                        style: TextStyle(fontSize: 20)),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                ),
              ]),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // ROW 3
            children: [
              SizedBox(
                width: 200, // specific value

                child: RaisedButton(
                  color: Colors.deepPurpleAccent,
                  textColor: Colors.white,
                  onPressed: () {Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PaymentPage()),
                  );
                  },
                  child: const Text('Paiement', style: TextStyle(fontSize: 20)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
              SizedBox(
                width: 200, // specific value
                child: RaisedButton(
                  color: Colors.deepPurpleAccent,
                  textColor: Colors.white,
                  onPressed: () {Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ThemeeListPage()),
                  );
                  },
                  child: const Text('Thème', style: TextStyle(fontSize: 20)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // ROW 3.bis
            children: [
              SizedBox(
                width: 200, // specific value

                child: RaisedButton(
                  color: Colors.deepPurpleAccent,
                  textColor: Colors.white,
                  onPressed: () {Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => UserListPage()),
                  );
                  },
                  child:
                      const Text('Utilisateur', style: TextStyle(fontSize: 20)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
              SizedBox(
                width: 200, // specific value
                child: RaisedButton(
                  color: Colors.deepPurpleAccent,
                  onPressed: () {Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => VocabularyListPage()),
                  );
                  },
                  textColor: Colors.white,
                  child:
                      const Text('Vocabulaire', style: TextStyle(fontSize: 20)),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                ),
              ),
            ],
          ),
        ]));
  }
}



