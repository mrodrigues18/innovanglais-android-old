import 'package:flutter/material.dart';
import 'package:innovanglais/fragments/account_pages/view_account.dart';
import 'package:innovanglais/fragments/index_page/index_page.dart';
import 'package:innovanglais/fragments/test_pages/list_tests.dart';
import 'package:innovanglais/modele/User.dart';
import 'package:innovanglais/pages/admin_page.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class HomePage extends StatefulWidget {
  final User currentUser;
  HomePage({Key? key, required this.currentUser}) : super(key: key);

  final drawerItemsAdministrateur = [
    new DrawerItem("Accueil", Icons.home),
    new DrawerItem("Tests", Icons.assignment),
    new DrawerItem("Administration", Icons.assignment),
    new DrawerItem("Mon compte", Icons.person_outline)
  ];

  final drawerItemsUtilisateur = [
    new DrawerItem("Accueil", Icons.home),
    new DrawerItem("Tests", Icons.assignment),
    new DrawerItem("Mon compte", Icons.person_outline)
  ];

  @override
  State<StatefulWidget> createState() {
    return new _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    if(this.widget.currentUser.roleUser == 'Administrateur') {
      switch(pos) {
        case 0:
          return IndexPage();
        case 1:
          return TestListPage();
        case 2:
          return AdminListPage();
        case 3:
          return AccountViewPage(currentUser: this.widget.currentUser,);
        default:
          return Center(child: Text("Une erreur s'est produite."),);
      }
    } else {
      switch(pos) {
        case 0:
          return IndexPage();
        case 1:
          return TestListPage();
        case 2:
          return AccountViewPage(currentUser: this.widget.currentUser,);
        default:
          return Center(child: Text("Une erreur s'est produite."),);
      }
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];
    if(this.widget.currentUser.roleUser == 'Administrateur') {
      for (var i = 0; i < widget.drawerItemsAdministrateur.length; i++) {
        var d = widget.drawerItemsAdministrateur[i];
        drawerOptions.add(
            new ListTile(
              leading: new Icon(d.icon),
              title: new Text(d.title),
              selected: i == _selectedDrawerIndex,
              onTap: () {
                // Permet de faire disparaître la SideBar après le cliqué sur l'un des menus
                Navigator.pop(context);
                _onSelectItem(i);
              },
            )
        );
      }

      return new Scaffold(
        appBar: new AppBar(
          title: new Text('Innov\'Anglais - ' + widget.drawerItemsAdministrateur[_selectedDrawerIndex].title),
        ),
        drawer: ListView(
          children: <Widget>[
            new Card(
              child: new Column(
                children: <Widget>[
                  new UserAccountsDrawerHeader(
                    accountName: Text("${this.widget.currentUser.usernameUser}"),
                    accountEmail: Text("${this.widget.currentUser.email}"),
                    currentAccountPicture: CircleAvatar(
                      backgroundColor: Colors.white,
                      child:
                      Image.network(
                        'http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/logo_innovanglais.png',
                        height: 100.0,
                        width: 100.0,
                      ),
                    ),
                  ),
                  new Column(
                    children: drawerOptions,
                  ),
                ],
              ),
            )
          ],
        ),
        body: _getDrawerItemWidget(_selectedDrawerIndex),
      );
    } else {
      for (var i = 0; i < widget.drawerItemsUtilisateur.length; i++) {
        var d = widget.drawerItemsUtilisateur[i];
        drawerOptions.add(
            new ListTile(
              leading: new Icon(d.icon),
              title: new Text(d.title),
              selected: i == _selectedDrawerIndex,
              onTap: () {
                // Permet de faire disparaître la SideBar après le cliqué sur l'un des menus
                Navigator.pop(context);
                _onSelectItem(i);
              },
            )
        );
      }
      return new Scaffold(
        appBar: new AppBar(
          title: new Text('Innov\'Anglais - ' + widget.drawerItemsUtilisateur[_selectedDrawerIndex].title),
        ),
        drawer: ListView(
          children: <Widget>[
            new Card(
              child: new Column(
                children: <Widget>[
                  new UserAccountsDrawerHeader(
                    accountName: Text("${this.widget.currentUser.usernameUser}"),
                    accountEmail: Text("${this.widget.currentUser.email}"),
                    currentAccountPicture: CircleAvatar(
                      backgroundColor: Colors.white,
                      child:
                      Image.network(
                        'http://serveur1.arras-sio.com/symfony4-4017/innovAnglais/public/img/logo_innovanglais.png',
                        height: 100.0,
                        width: 100.0,
                      ),
                    ),
                  ),
                  new Column(
                    children: drawerOptions,
                  ),
                ],
              ),
            )
          ],
        ),
        body: _getDrawerItemWidget(_selectedDrawerIndex),
      );
    }
  }
}