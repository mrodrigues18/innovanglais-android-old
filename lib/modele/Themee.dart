class Themee {
  String idThemee;
  String libelleThemee;

  Themee({required this.idThemee, required this.libelleThemee});

  factory Themee.fromJson(Map<String, dynamic> json) {
    return Themee(
        idThemee: json['idTheme'],
        libelleThemee: json['libelleTheme']
    );
  }
}