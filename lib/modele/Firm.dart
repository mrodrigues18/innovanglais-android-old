class Firm {
  String idFirm;
  String nameFirm;

  Firm({required this.idFirm, required this.nameFirm});

  factory Firm.fromJson(Map<String, dynamic> json) {
    return Firm(idFirm: json['idFirm'], nameFirm: json['nameFirm']);
  }
}
