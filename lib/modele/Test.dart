class Test {
  String themeLabel;
  String idLevel;
  String levelLabel;
  String testLabel;

  Test({required this.themeLabel, required this.idLevel, required this.levelLabel, required this.testLabel});

  factory Test.fromJson(Map<String, dynamic> json) {
    return Test(
        idLevel: json['idLevel'],
        themeLabel: json['themeLabel'],
        levelLabel: json['levelLabel'],
        testLabel: json['testLabel']
    );
  }
}