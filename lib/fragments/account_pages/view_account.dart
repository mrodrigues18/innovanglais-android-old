import 'dart:io';
import 'package:innovanglais/modele/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class AccountViewPage extends StatefulWidget {
  final User currentUser;
  AccountViewPage({Key? key, required this.currentUser}) : super(key: key);

  @override
  _AccountViewPageState createState() => _AccountViewPageState();
}

class _AccountViewPageState extends State<AccountViewPage> {

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if(kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: new Center(
          child: Card(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    const Text("Nom complet: ", style: TextStyle(fontSize: 20.0),),
                    new Text("${this.widget.currentUser.first_name_user} ${this.widget.currentUser.last_name_user.toUpperCase()}", style: TextStyle(fontSize: 24.0), softWrap: true,),
                  ],
                ),
                new Row(
                  children: <Widget>[
                    const Text("Niveau: "),
                    new Text("${this.widget.currentUser.levelUser}"),
                          ],
                        ),
                      ],
                    ),
                ),
            ),
      ),
    );
  }
}