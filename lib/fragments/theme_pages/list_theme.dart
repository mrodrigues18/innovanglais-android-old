import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:innovanglais/modele/Themee.dart';
import 'package:shimmer/shimmer.dart';

class ThemeeListPage extends StatefulWidget {
  @override
  _ThemeeListPageState createState() => _ThemeeListPageState();
}

class _ThemeeListPageState extends State<ThemeeListPage> {
  final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/theme_JSON");

  Future<List<Themee>> fetchThemees() async {
    var response = await http.get(url);
    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<Themee> listOfThemees = items.map<Themee>((json) {
        return Themee.fromJson(json);
      }).toList();
      return listOfThemees;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Innov\'Anglais - Thèmes')),
      body: new Container(
        child: new Center(
          child: new FutureBuilder<List<Themee>>(
            future: fetchThemees(),
            builder: (context, snapshot) {
              /// Affiche un squelette tant que les données ne sont pas récuperées
              if (!snapshot.hasData)
                return ListView(
                  children: <Widget>[
                    Container(
                      child: Center(
                        child: Shimmer.fromColors(
                          baseColor: Colors.grey[300]!,
                          highlightColor: Colors.grey[100]!,
                          child: Column(
                            children: <int>[0, 1, 2, 3, 4, 5, 6, 7]
                                .map((_) => Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 10.0,
                                          top: 10.0,
                                          right: 10.0,
                                          left: 10.0),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 48.0,
                                            height: 48.0,
                                            color: Colors.white,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 8.0),
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  width: double.infinity,
                                                  height: 8.0,
                                                  color: Colors.white,
                                                ),
                                                const Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 2.0),
                                                ),
                                                Container(
                                                  width: double.infinity,
                                                  height: 8.0,
                                                  color: Colors.white,
                                                ),
                                                const Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 2.0),
                                                ),
                                                Container(
                                                  width: 40.0,
                                                  height: 8.0,
                                                  color: Colors.white,
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ))
                                .toList(),
                          ),
                        ),
                      ),
                    )
                  ],
                );

              /// Si table vide dans la BDD
              if (snapshot.data!.isEmpty) {
                return Center(
                  child: Text("Aucun theme"),
                );
              }
              return ListView(
                children: snapshot.data!
                    .map((data) => Card(
                          child: new Center(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                new CupertinoButton(
                                  child: new ListTile(
                                    leading: Text(
                                      data.libelleThemee,
                                      style: new TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(builder: (context) {
                                          return Container();
                                        })
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ))
                    .toList(),
              );
            },
          ),
        ),
      ),
    );
  }
}
