import 'package:flutter/material.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> with TickerProviderStateMixin {
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          Row(
              //ROW 1
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 50.0, 0, 50.0),
                    child: const Text('Bienvenue',
                        style:
                            TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                  ),
                ),
              ]),
          Row(
              //ROW 1
              mainAxisAlignment: MainAxisAlignment.center,
          children: [
                SizedBox(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0,20,0,0),
                    child: const Text('Sur InnovAnglais vous retrouvez,\n des tests pour améliorer votre anglais.',
                        maxLines: 20,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                ),
              ]),
        ]));
  }
}
