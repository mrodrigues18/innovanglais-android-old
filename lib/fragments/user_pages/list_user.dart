import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:innovanglais/modele/User.dart';
import 'package:shimmer/shimmer.dart';

class UserListPage extends StatefulWidget {
  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  final Uri url = Uri.http("serveur1.arras-sio.com", "symfony4-4017/innovAnglais/public/user_JSON");

  Future<List<User>> fetchUsers() async {
    var response = await http.get(url);
    if (response.statusCode == 200) {
      final items = json.decode(response.body).cast<Map<String, dynamic>>();
      List<User> listOfUsers = items.map<User>((json) {
        return User.fromJson(json);
      }).toList();
      return listOfUsers;
    } else {
      throw Exception('Une erreur s\'est produite.');
    }
  }

  void main() {
    FlutterError.onError = (FlutterErrorDetails details) {
      FlutterError.dumpErrorToConsole(details);
      if (kReleaseMode) exit(1);
    };
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Innov\'Anglais - Utilisateurs')),
      body: new Container(
        child: new Center(
          child: new FutureBuilder<List<User>>(
            future: fetchUsers(),
            builder: (context, snapshot) {
              /// Affiche un squelette tant que les données ne sont pas récuperées
              if (!snapshot.hasData)
                return ListView(
                  children: <Widget>[
                    Container(
                      child: Center(
                        child: Shimmer.fromColors(
                          baseColor: Colors.grey[300]!,
                          highlightColor: Colors.grey[100]!,
                          child: Column(
                            children: <int>[0, 1, 2, 3, 4, 5, 6, 7]
                                .map((_) => Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 10.0,
                                          top: 10.0,
                                          right: 10.0,
                                          left: 10.0),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 48.0,
                                            height: 48.0,
                                            color: Colors.white,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 8.0),
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  width: double.infinity,
                                                  height: 8.0,
                                                  color: Colors.white,
                                                ),
                                                const Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 2.0),
                                                ),
                                                Container(
                                                  width: double.infinity,
                                                  height: 8.0,
                                                  color: Colors.white,
                                                ),
                                                const Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 2.0),
                                                ),
                                                Container(
                                                  width: 40.0,
                                                  height: 8.0,
                                                  color: Colors.white,
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ))
                                .toList(),
                          ),
                        ),
                      ),
                    )
                  ],
                );

              /// Si table vide dans la BDD
              if (snapshot.data!.isEmpty) {
                return Center(
                  child: Text("Aucun utilisateur"),
                );
              }
              return ListView(
                children: snapshot.data!
                    .map((data) => Card(
                          child: new Center(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                new CupertinoButton(
                                  child: new ListTile(
                                    leading: Text(
                                      data.usernameUser,
                                      style: new TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    title: (data.libellePaiement == null && data.roleUser=="[\"ROLE_ADMIN\"]")
                                        ? Text("Utilisateur : " +
                                            '\n' +
                                        "Nom : " + data.first_name_user +
                                            '\n' +
                                        "Prénom : " + data.last_name_user +
                                            '\n' +
                                        "Rôle : " + "Administrateur" +
                                        '\n' +
                                        "Âge : " + data.ageUser +
                                        '\n' +
                                        "Niveau : " + data.levelUser +
                                        '\n' +
                                        "Entreprise : " + data.firmUser! +
                                            "   ")
                                        : Text(data.usernameUser +
                                            "   " +
                                            data.first_name_user +
                                            "   " +
                                            data.last_name_user +
                                            "   " +
                                            data.roleUser +
                                            "   " +
                                            data.last_name_user +
                                            "   " +
                                            data.ageUser +
                                            "   " +
                                            data.levelUser +
                                            "   " +
                                            data.libellePaiement! +
                                            "   " +
                                            data.firmUser! +
                                            "   "),
                                  ),
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: Text("Dialog title"),
                                          content: Text("Username:    " + data.usernameUser +
                                              '\n' +
                                              data.first_name_user +
                                              '\n' +
                                              data.last_name_user +
                                              '\n' +
                                              data.roleUser +
                                              "   " +
                                              data.last_name_user +
                                              "   " +
                                              data.ageUser +
                                              "   " +
                                              data.levelUser +
                                              "   " +
                                              data.firmUser! +
                                              "   "),
                                        );
                                      },
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        )).toList(),
              );
            },
          ),
        ),
      ),
    );
  }
}
